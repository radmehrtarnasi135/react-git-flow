import { useState } from 'react';
import './App.css';
import Btn from './components/btn-component/Btn.component';
import NumText from './components/num-text-component/NumText.component';

const App = () => {
  const [num, setNum] = useState(0);

  const addOne = () => {
    setNum(num + 1)
  }

  return(
    <div className="App">
      <NumText>{num}</NumText>
      <Btn onClick={addOne}>click here</Btn>
    </div>
  )
}


export default App;