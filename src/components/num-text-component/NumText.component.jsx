import styled from "styled-components"

const NumText = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 4rem;
    color: #333;
    padding: 2rem;
`;

export default NumText;