import styled from 'styled-components'

const Btn = styled.a`
    color: #fff;
    background-color: #000;
    border-radius: 5px;
    border: none;
    font-size: 1.5rem;
    padding: .5rem 1rem;
    cursor: pointer;
    
    &:hover {
        background-color: #333;
    }
`;

export default Btn;